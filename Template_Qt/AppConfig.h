#pragma once
#include <string>
#include <memory>

class AppConfig
{
public:
	static AppConfig* instance();

	std::string ip;
	unsigned short port;
	std::string cmdline;

private:
	AppConfig();
	AppConfig(const AppConfig&) = delete;
	virtual ~AppConfig() = default;

	void readConfigFile();

	static AppConfig* m_ins;
	static std::unique_ptr<AppConfig> m_ins_ptr;
	friend std::unique_ptr < AppConfig > ;
	friend std::default_delete < AppConfig > ;
};

