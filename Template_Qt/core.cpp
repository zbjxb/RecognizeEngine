// core.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "core.h"
#include "NativeScene.h"
#include <algorithm>
#include <metaioSDK/IMetaioSDKCallback.h>
#include <metaioSDK/IMetaioSDKWin32.h>
#include <metaioSDK/GestureHandler.h>

namespace RecognizeEngine
{
	// 有关类定义的信息，请参阅 core.h
	CCore::CCore() :
		bKeepRunning(false),
		bIsRunning(false),
		pMetaioSDK(nullptr)
	{
	}

	CCore::~CCore()
	{
		Stop();
	}

	void CCore::Start()
	{
		if (IsRunning())
		{
			return;
		}
		bKeepRunning = true;
		service = new std::thread(&CCore::Run, this);
	}

	void CCore::Restart()
	{
		if (IsRunning())
		{
			Stop();
		}
		Start();
	}

	void CCore::Run()
	{
		bIsRunning = true;

		while (bKeepRunning)
		{
			RecognizeOneFrame();
			ReportStatus();
		}

		bIsRunning = false;
	}

	void CCore::Stop()
	{
		if (IsRunning())
		{
			bKeepRunning = false;
			service->join();
			delete service;
			service = nullptr;
		}
	}

	bool CCore::IsRunning() const
	{
		return bIsRunning;
	}

	void CCore::SetSampleInterval(int _interval)
	{
		interval = _interval;
	}

	void CCore::AddSubscriber(CSubscriber* subscriber)
	{
		// 如果允许运作时添加订阅，则这里需要考虑并发访问控制
		if (std::find(subscribers.begin(), subscribers.end(), subscriber) == subscribers.end())
			subscribers.push_back(subscriber);
	}

	void CCore::RemoveSubscriber(CSubscriber* subscriber)
	{
		// 如果允许运作时添加订阅，则这里需要考虑并发访问控制
		subscribers.remove(subscriber);
	}

	void CCore::RecognizeOneFrame()
	{
		kinectConnector.Update();

		// 向metaio里传递kinect捕捉的image
		if (pMetaioSDK)
		{
			INT64 timestamp;
			unsigned char* img_buff = kinectConnector.GetImageSource()->GetOneImage(timestamp);
			metaio::ImageStruct imgStruct(
				img_buff,
				CImageSource::cColorWidth,
				CImageSource::cColorHeight,
				metaio::common::ECF_RGBA8,
				true,
				timestamp);
			pMetaioSDK->setImage(imgStruct);
		}
	}

	void CCore::ReportStatus()
	{

	}

	void CCore::EnterEnergySavingMode()
	{

	}

	void CCore::LeaveEnergySavingMode()
	{

	}

	void CCore::SetMetaio(metaio::IMetaioSDKWin32* _pMetaioSDK)
	{
		pMetaioSDK = _pMetaioSDK;
	}

	CSubscriber::CSubscriber(void)
	{

	}

	void CSubscriber::Subscribe(CCore* core)
	{
		if (core) {
			core->AddSubscriber(this);
			engine = core;
		}
	}

	void CSubscriber::Unsubscribe()
	{
		if (engine)
			engine->RemoveSubscriber(this);
	}

	void CSubscriber::NotifyMail(const CStatus& mail)
	{

	}

	CSubscriber::~CSubscriber(void)
	{
		Unsubscribe();
	}

}

