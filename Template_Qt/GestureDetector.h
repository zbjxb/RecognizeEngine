#pragma once
#include <string>
#include <vector>
#include <chrono>
#include <Kinect.VisualGestureBuilder.h>
#include "GestureManager.h"

using namespace std::chrono;

namespace RecognizeEngine
{
	class GestureDetector
	{
	public:
		GestureDetector(IKinectSensor* sensor);
		~GestureDetector();

		WAITABLE_HANDLE m_hTrackingIdLostEvent;
		WAITABLE_HANDLE m_hGestureFrameArrivedEvent;

		/// <summary>
		/// Main processing function
		/// </summary>
		HRESULT						Update(IBody* body);

		bool IsCheckingEvent() const { return m_bKeepCheckingEvent;  }
		void StopCheckEvent() { m_bKeepCheckingEvent = false;  }
		static DWORD WINAPI CheckEventThreadProc(LPVOID lpParam);

		IVisualGestureBuilderFrameReader* VGBFrameReader();
		IVisualGestureBuilderFrameSource* VGBFrameSource();

		UINT64 GetTrackingId() const;
		void SetTrackingId(UINT64 id);

		bool IsPaused() const;
		void SetPaused(bool val);

		void Source_TrackingIdLost(ITrackingIdLostEventArgs* e);
		void Reader_GestureFrameArrived(IVisualGestureBuilderFrameArrivedEventArgs* e);

	private:
		std::wstring gestureDatabase;
		std::wstring jumpGestureName;

		GestureManager	gestureMgr;
		Joint			m_joints[JointType_Count];

		UINT m_nGesturesCount;
		IGesture** m_arrayGestures;
		CComPtr<IVisualGestureBuilderFrameSource> vgbFrameSource;
		CComPtr<IVisualGestureBuilderFrameReader> vgbFrameReader;

		bool m_bKeepCheckingEvent;
		HANDLE m_hCheckEventThreadHandle;
	};
}
