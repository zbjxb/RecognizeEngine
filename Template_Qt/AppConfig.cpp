#include "StdAfx.h"
#include "AppConfig.h"
#include "prop.h"

AppConfig* AppConfig::m_ins = nullptr;
std::unique_ptr<AppConfig> AppConfig::m_ins_ptr;

AppConfig::AppConfig()
{
	m_ins_ptr = std::unique_ptr<AppConfig>(this);
}

AppConfig* AppConfig::instance()
{
	if (m_ins)
		return m_ins;
	m_ins = new AppConfig();
	m_ins->readConfigFile();
	return m_ins;
}

void AppConfig::readConfigFile()
{
	prop config(AssetPathA(config.prop));
	ip = config.getstring("ip", "127.0.0.1");
	port = config.getint("port", 18001);
	cmdline = config.getstring("cmdline");
	printf("ip: %s, port: %d, cmdline: %s\n", ip.c_str(), port, cmdline.c_str());
}
