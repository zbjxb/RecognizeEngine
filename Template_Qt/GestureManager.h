#pragma once
#include <list>
#include <functional>
#include <chrono>

using namespace std::chrono;

namespace RecognizeEngine
{
	struct GesturePoint
	{
		CameraSpacePoint position;
		time_point<system_clock> time;
	};

	enum GestureDirection
	{
		GestureDirection_X,
		GestureDirection_Y
	};

	struct GestureParameter
	{
		JointType jointType;
		GestureDirection gestureDirection;
		double swipeLength;
		double swipeDeviation;
		int swipeTime;
		std::list<GesturePoint> gesturePoints;

		void InitializeGestureParameter(JointType type, GestureDirection direction, double swipeLength, double swipeDeviation, int swipeTime);
		void ResetGesturePoint(int point_count = 0);
		bool IsGestureDetected(Joint* joints, time_point<system_clock>& timepoint);
	};

	class GestureManager
	{
	public:
		GestureManager();
		~GestureManager();

		void RegisterGestureParamter(GestureParameter& param);
		void RegisterGestureParamters(std::list<GestureParameter>& params);
		void StartGesturePointTracking();
		void StopGesturePointTracking();
		bool IsGesturePointTrackingEnabled() const;
		void ResetGesturePoint(int point_count = 0);
		void HandleGestureTracking(Joint* joints);
		void RegisterSwipeDetected(std::function<void()>&& func);

	private:
		bool gesturePointTrackingEnabled;
		std::list<GestureParameter> gestureParams;
		std::list<std::function<void()>>  swipeDetected;
	};

}
