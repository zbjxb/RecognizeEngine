#pragma once
#include <vector>
#include <memory>
#include "GestureDetector.h"
#include "ImageSource.h"

namespace RecognizeEngine
{
	const UINT MAX_BODY = 6;

	class KinectConnector
	{
	public:
		KinectConnector();
		~KinectConnector();

		/// <summary>
		/// Main processing function
		/// </summary>
		void                    Update();

		CImageSource*			GetImageSource() { return &m_imageSource;  }

	private:
		/// <summary>
		/// Initializes the default Kinect sensor
		/// </summary>
		/// <returns>S_OK on success, otherwise failure code</returns>
		HRESULT                 InitializeDefaultSensor();

		void					InitializeBodies();
		void					UninitializeBodies();

	private:
		// Current Kinect
		CComPtr<IKinectSensor>          m_pKinectSensor;

		// MultiSource frame reader
		CComPtr<IMultiSourceFrameReader> m_pMultiSourceFrameReader;

		// bodies
		UINT m_nBodyCount;
		IBody**	m_pBodies;

		// Gesture Detectors
		std::vector<std::unique_ptr<GestureDetector>> m_vecGestureDetectors;

		// Color image source
		CImageSource m_imageSource;
	};
}
