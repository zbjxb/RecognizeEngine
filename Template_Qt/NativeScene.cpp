#include "stdafx.h"
#include "NativeScene.h"

#include <QtWidgets/QGraphicsSceneMouseEvent>

#include <metaioSDK/GestureHandler.h>

#include "insNetwork/iInsTcpClient.h"
#include "insApi.h"

template<>
struct std::less < metaio::stlcompat::String >
{
	bool operator()(const metaio::stlcompat::String& _Left, const metaio::stlcompat::String& _Right) const
	{	// apply operator< to operands
		return (_Left.compare(_Right) < 0);
	}
};

NativeScene::NativeScene(QObject *parent) :
	SceneBase(parent)
{
	/*m_earth = 0;
	m_earthOcclusion = 0;
	m_earthIndicators = 0;
	m_earthOpened = false;*/
}


NativeScene::~NativeScene()
{
}


void NativeScene::loadContent()
{
	// TODO: Load your own content here

	/*if (!m_pMetaioSDK->setTrackingConfiguration(metaio::Path::fromUTF8("../../../../../templatesContent_crossplatform/TrackingData_MarkerlessFast.xml")))
		qCritical("Failed to load tracking configuration");
	
	const float scale = 11.f;
	const metaio::Rotation rotation = metaio::Rotation(metaio::Vector3d(static_cast<float>(M_PI/2.0), 0.f, 0.f));

	m_earth = m_pMetaioSDK->createGeometry(metaio::Path::fromUTF8("../../../../../templatesContent_crossplatform/Earth.zip"));
	if (m_earth)
	{
		m_earth->setScale(scale);
		m_earth->setRotation(rotation);
	}
	else
		qCritical("Failed to load earth model file");

	m_earthOcclusion = m_pMetaioSDK->createGeometry(metaio::Path::fromUTF8("../../../../../templatesContent_crossplatform/Earth_Occlusion.zip"));
	if (m_earthOcclusion)
	{
		m_earthOcclusion->setScale(scale);
		m_earthOcclusion->setRotation(rotation);
		m_earthOcclusion->setOcclusionMode(true);
	}
	else
		qCritical("Failed to load earth occlusion model file");

	m_earthIndicators = m_pMetaioSDK->createGeometry(metaio::Path::fromUTF8("../../../../../templatesContent_crossplatform/EarthIndicators.zip"));
	if (m_earthIndicators)
	{
		m_earthIndicators->setScale(scale);
		m_earthIndicators->setRotation(rotation);
	}
	else
		qCritical("Failed to load earth indicators model file");*/

	if (!m_pMetaioSDK->setTrackingConfiguration(metaio::Path::fromFSEncoding(AssetPathA(TrackingData_PictureMarker.xml))))
		qCritical("Failed to load tracking configuration");

	/*m_pFishImagePlane = m_pMetaioSDK->createGeometryFromImage(metaio::Path::fromFSEncoding(AssetPathA(fish.jpg)));
	if (m_pFishImagePlane)
	{
		m_pFishImagePlane->setScale(5.0f);
		m_pFishImagePlane->setVisible(true);
	}
	else
		qCritical("Failed to load image frame model");*/

	/*m_pCarotaImagePlane = m_pMetaioSDK->createGeometryFromImage(metaio::Path::fromFSEncoding("../../../../../templatesContent_crossplatform/carota.jpg"));
	if (m_pCarotaImagePlane)
	{
		m_pCarotaImagePlane->setScale(5.0f);
		m_pCarotaImagePlane->setVisible(true);
	}
	else
		qCritical("Failed to load image frame model");*/
}


void NativeScene::performRendering()
{
	m_pMetaioSDK->render();
}

void NativeScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	super::mousePressEvent(mouseEvent);

	const int x = mouseEvent->scenePos().x();
	const int y = mouseEvent->scenePos().y();

	// Forward event to gesture handler
	if (m_pGestureHandler)
		m_pGestureHandler->touchesBegan(x, y);

	/*metaio::IGeometry* geometry = m_pMetaioSDK->getGeometryFromViewportCoordinates(x, y);

	if (geometry != NULL)
	{
		onGeometryTouched(geometry);
	}*/
}

void NativeScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	super::mouseMoveEvent(mouseEvent);

	const int x = mouseEvent->scenePos().x();
	const int y = mouseEvent->scenePos().y();

	// Forward event to gesture handler (needed for drag gesture, just like the mouse press/release events)
	if (m_pGestureHandler)
		m_pGestureHandler->touchesMoved(x, y);
}

void NativeScene::mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	super::mouseReleaseEvent(mouseEvent);

	// See comment in mouseMoveEvent()
	const int x = mouseEvent->scenePos().x();
	const int y = mouseEvent->scenePos().y();

	// Forward event to gesture handler
	if (m_pGestureHandler)
		m_pGestureHandler->touchesEnded(x, y);
}

void NativeScene::onTrackingEvent(const metaio::stlcompat::Vector< metaio::TrackingValues > &trackingValues)
{
	static std::map<metaio::stlcompat::String, int> detected_obj;

	for (unsigned int i = 0; i < trackingValues.size(); i++)
	{
		const metaio::TrackingValues tv = trackingValues[i];
		if (tv.isTrackingState() && tv.quality >= 0.5f)
		{
			auto iter = detected_obj.find(tv.cosName);
			if (iter != detected_obj.end())
				++iter->second;
			else
				iter = detected_obj.insert(std::make_pair(tv.cosName, 1)).first;

			printf("%s found %d.\n", tv.cosName.c_str(), iter->second);

			char szBuffer[1024];
			sprintf_s(szBuffer, 1024, \
			"{\"Operation\":\"EventTrigger\", \"Name\":\"%s\"}", tv.cosName.c_str());

			/*sprintf_s(szBuffer, 1024, \
				"{\"Operation\":\"SelectActor\", \"ObjectType\":\"%s\", \"Name\":\"%s\"}", \
				"CAMERA", "CameraActor");*/

			std::wstring lwstrData;
			ins::AnsiToUnicode16(szBuffer, lwstrData);
			int iDataLen = ins::UnicodeToUTF8(lwstrData.c_str(), szBuffer, 1024);
			if (!TcpClient->SendData(szBuffer, iDataLen))
			{
				qCritical("SendData Error!");
				break;
			}

			/*static char buffs[MAX_PATH];
			sprintf_s(buffs, MAX_PATH, AssetPathA(screenshot/%d.jpg), std::chrono::system_clock::now().time_since_epoch());
			m_pMetaioSDK->requestScreenshot(metaio::Path::fromFSEncoding(buffs));*/
		}
	}
}

//void NativeScene::onGeometryTouched(metaio::IGeometry* geometry)
//{
//	if (geometry != m_earthOcclusion)
//	{
//		if (!m_earthOpened)
//		{
//			m_earth->startAnimation("Open", false);
//			m_earthIndicators->startAnimation("Grow", false);
//			m_earthOpened = true;
//		}
//		else
//		{
//			m_earth->startAnimation("Close", false);
//			m_earthIndicators->startAnimation("Shrink", false);
//			m_earthOpened = false;
//		}
//	}
//}
