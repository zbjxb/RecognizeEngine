#pragma once

#include <thread>
#include <list>
#include "KinectConnector.h"

namespace metaio
{
	class IMetaioSDKWin32;
}

namespace RecognizeEngine
{
	class CStatus;
	class CCore;

	/**
	需要从识别引擎获取识别状态通知的用户，需要派生此类，并重载NotifyMail方法。
	*/
	class CSubscriber
	{
	public:
		CSubscriber(void);
		virtual ~CSubscriber(void);

		virtual void NotifyMail(const CStatus& mail);

		// 订阅服务
		void Subscribe(CCore* core);
		void Unsubscribe();

	private:
		CCore* engine;
		//FnMailbox box;
	};


	/**
	识别引擎的结果以CStatus的对象给出。引擎负责该对象的生命管理。
	*/
	class CStatus
	{
	public:
		CStatus(void);
	};

	class CCore 
	{
	public:
		CCore(void);
		virtual ~CCore(void);
		// TODO:  在此添加您的方法。

	public:
		void SetSampleInterval(int interval);
		//void SetImageSource(CImageSource* src);
		void SetMetaio(metaio::IMetaioSDKWin32* pMetaioSDK);
		void RecognizeOneFrame();

		void Start();
		void Restart();
		void Stop();
		bool IsRunning() const;

		void AddSubscriber(CSubscriber* subscriber);
		void RemoveSubscriber(CSubscriber* subscriber);

	private:
		void ReportStatus();
		void EnterEnergySavingMode();
		void LeaveEnergySavingMode();

		void Run();

	private:
		bool bKeepRunning;
		bool bIsRunning;
		std::thread* service;

		int interval;

		std::list<CSubscriber*> subscribers;


		KinectConnector kinectConnector;

		//////////////////////////////////////////////////////////////////////////
		// metaio
		metaio::IMetaioSDKWin32* pMetaioSDK;
	};
};
