#pragma once

namespace RecognizeEngine
{
	/**
	主要负责彩色图像的处理工作
	*/

	class CImageSource
	{
	public:
		static const int        cColorWidth  = 1920;
		static const int        cColorHeight = 1080;

		/// <summary>
		/// Constructor
		/// </summary>
		CImageSource();

		/// <summary>
		/// Destructor
		/// </summary>
		~CImageSource();

		/// <summary>
		/// Main processing function
		/// </summary>
		HRESULT						Update(IColorFrame* pColorFrame);

		// image buffer
		unsigned char* GetOneImage(INT64& timestamp);

	private:
		HWND                    m_hWnd;
		INT64                   m_nStartTime;
		INT64					m_nTimestamp;
		INT64                   m_nLastCounter;
		double                  m_fFreq;
		INT64                   m_nNextStatusTime;
		DWORD                   m_nFramesSinceUpdate;
		bool                    m_bSaveScreenshot;


		// Direct2D
		//ImageRenderer*          m_pDrawColor;
		//ID2D1Factory*           m_pD2DFactory;
		RGBQUAD*                m_pColorRGBX;


		/// <summary>
		/// Handle new color data
		/// <param name="nTime">timestamp of frame</param>
		/// <param name="pBuffer">pointer to frame data</param>
		/// <param name="nWidth">width (in pixels) of input image data</param>
		/// <param name="nHeight">height (in pixels) of input image data</param>
		/// </summary>
		void                    ProcessColor(INT64 nTime, RGBQUAD* pBuffer, int nWidth, int nHeight);

		/// <summary>
		/// Get the name of the file where screenshot will be stored.
		/// </summary>
		/// <param name="lpszFilePath">string buffer that will receive screenshot file name.</param>
		/// <param name="nFilePathSize">number of characters in lpszFilePath string buffer.</param>
		/// <returns>
		/// S_OK on success, otherwise failure code.
		/// </returns>
		HRESULT                 GetScreenshotFileName(_Out_writes_z_(nFilePathSize) LPWSTR lpszFilePath, UINT nFilePathSize);

		/// <summary>
		/// Save passed in image data to disk as a bitmap
		/// </summary>
		/// <param name="pBitmapBits">image data to save</param>
		/// <param name="lWidth">width (in pixels) of input image data</param>
		/// <param name="lHeight">height (in pixels) of input image data</param>
		/// <param name="wBitsPerPixel">bits per pixel of image data</param>
		/// <param name="lpszFilePath">full file path to output bitmap to</param>
		/// <returns>indicates success or failure</returns>
		HRESULT                 SaveBitmapToFile(BYTE* pBitmapBits, LONG lWidth, LONG lHeight, WORD wBitsPerPixel, LPCWSTR lpszFilePath);
	};

}

