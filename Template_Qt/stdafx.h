// Precompiled header. Include any headers here that do not change often - this will improve compiler performance.

#define WIN32_LEAN_AND_MEAN

// Kinect Header files
#include <Shlobj.h>
#include <Kinect.h>
#include <tchar.h>
#include <atlbase.h>

// Safe release for interfaces
template<class Interface>
inline void SafeRelease(Interface *& pInterfaceToRelease)
{
	if (pInterfaceToRelease != NULL)
	{
		pInterfaceToRelease->Release();
		pInterfaceToRelease = NULL;
	}
}

template<class F>
struct Final_action
{
	Final_action(F f) :clean(f), _discard(false) {}
	~Final_action(){ if (!_discard) clean(); }
	void discard() { _discard = true; }
	F clean;
	bool _discard;
};

template<class F>
Final_action<F> finally(F f)
{
	return Final_action<F>(f);
}

#ifdef _DEBUG
#define AssetPathA(x) "../../../../../templatesContent_crossplatform/"#x
#define AssetPathW(x) L##"../../../../../templatesContent_crossplatform/"#x
#else
#define AssetPathA(x) "templatesContent_crossplatform/"#x
#define AssetPathW(x) L##"templatesContent_crossplatform/"#x
#endif

#ifdef UNICODE
#define AssetPath AssetPathW
#else
#define AssetPath AssetPathA
#endif

class IInsTcpClient;
extern IInsTcpClient* TcpClient;
