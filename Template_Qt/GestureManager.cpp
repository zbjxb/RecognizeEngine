#include "StdAfx.h"
#include "GestureManager.h"
#include <algorithm>
#include <assert.h>

namespace RecognizeEngine
{
	GestureManager::GestureManager()
	{
	}

	GestureManager::~GestureManager()
	{
	}

	void GestureManager::RegisterGestureParamter(GestureParameter& param)
	{
		gestureParams.push_back(param);
	}

	void GestureManager::RegisterGestureParamters(std::list<GestureParameter>& params)
	{
		gestureParams.insert(gestureParams.end(), params.begin(), params.end());
	}

	void GestureManager::StartGesturePointTracking()
	{
		gesturePointTrackingEnabled = true;
	}

	void GestureManager::StopGesturePointTracking()
	{
		gesturePointTrackingEnabled = false;
	}

	bool GestureManager::IsGesturePointTrackingEnabled() const
	{
		return gesturePointTrackingEnabled;
	}

	void GestureManager::ResetGesturePoint(int point_count)
	{
		std::for_each(gestureParams.begin(), gestureParams.end(), [point_count](GestureParameter& i){i.ResetGesturePoint(point_count);});
	}

	void GestureManager::HandleGestureTracking(Joint* joints)
	{
		if (!gesturePointTrackingEnabled)
			return;

		int undetected_count = 0;
		auto timenow = system_clock::now();
		for (auto i = gestureParams.begin(); i != gestureParams.end(); i++) {
			if (!i->IsGestureDetected(joints, timenow)) {
				++undetected_count;
			}
		}
		if (undetected_count == 0) {
			ResetGesturePoint();

			if (!gestureParams.empty()) {
				if (!swipeDetected.empty()) {
					// ...
					for (auto i = swipeDetected.begin(); i != swipeDetected.end(); i++) {
						(*i)();
					}
				}
			}
		}
	}

	void GestureManager::RegisterSwipeDetected(std::function<void()>&& func)
	{
		swipeDetected.push_back(func);
	}

	void GestureParameter::InitializeGestureParameter(JointType type, GestureDirection direction, double swipeLength, double swipeDeviation, int swipeTime)
	{
		if (swipeLength + swipeDeviation + swipeTime == 0) {
			throw std::invalid_argument("invalid swipe gesture parameters");
		}

		this->jointType          = type;
		this->gestureDirection   = direction;
		this->swipeLength        = swipeLength;
		this->swipeDeviation     = swipeDeviation;
		this->swipeTime          = swipeTime;
	}

	void GestureParameter::ResetGesturePoint(int point_count /*= 0*/)
	{
		if (point_count < 1) {
			gesturePoints.clear();
			return;
		}

		auto begin = gesturePoints.begin();
		auto end = gesturePoints.begin();
		std::advance(end, point_count);
		gesturePoints.erase(begin, end);
	}

	bool GestureParameter::IsGestureDetected(Joint* joints, time_point<system_clock>& timepoint)
	{
		assert(joints && (jointType >= JointType_SpineBase) && (jointType < JointType_Count));
		Joint& joint = joints[jointType];

		GesturePoint newPoint{ joint.Position, timepoint };
		gesturePoints.push_back(newPoint);

		GesturePoint startPoint = gesturePoints.front();

		// check for deviation
		if (((gestureDirection == GestureDirection_Y) && abs(newPoint.position.X - startPoint.position.X) > swipeDeviation) ||
			((gestureDirection == GestureDirection_X) && abs(newPoint.position.Y - startPoint.position.Y) > swipeDeviation))
		{
			// printf("X out of bounds");
			//if (!swipeOutOfBoundDetected.empty()) {
				// ...
			//}
			ResetGesturePoint(gesturePoints.size());
			return false;
		}

		// check time
		int timespan = duration_cast<milliseconds>(newPoint.time - startPoint.time).count();
		if (timespan > swipeTime)
		{
			gesturePoints.erase(gesturePoints.begin());
			startPoint = gesturePoints.front();
		}

		if (gestureDirection == GestureDirection_Y) {
			if ((swipeLength < 0 && newPoint.position.Y - startPoint.position.Y < swipeLength) ||	// check to see if distance has been achieved swipe down
				(swipeLength > 0 && newPoint.position.Y - startPoint.position.Y > swipeLength))		// check to see if distance has been achieved swipe up
			{
				//gesturePoints.clear();	// 当所有跟踪的关节点都检测成功后统一释放

				// throw local event
				//if (!swipeDetected.empty()) {
				//	// ...
				//	for (auto i = swipeDetected.begin(); i != swipeDetected.end(); i++) {
				//		(*i)();
				//	}
				//}
				return true;
			}
		}
		else if (gestureDirection == GestureDirection_X) {
			if ((swipeLength < 0 && newPoint.position.X - startPoint.position.X < swipeLength) ||	// check to see if distance has been achieved swipe left 
				(swipeLength > 0 && newPoint.position.X - startPoint.position.X > swipeLength))		// check to see if distance has been achieved swipe right
			{
				//gesturePoints.clear();	// 当所有跟踪的关节点都检测成功后统一释放

				// throw local event
				//if (!swipeDetected.empty()) {
				//	// ...
				//	for (auto i = swipeDetected.begin(); i != swipeDetected.end(); i++) {
				//		(*i)();
				//	}
				//}
				return true;
			}
		}

		//if (yOutOfBoundsLength != 0 &&
		//	((yOutOfBoundsLength < 0 && newPoint.position.Y - initialSwipeY < yOutOfBoundsLength) ||	// swipe down
		//	(yOutOfBoundsLength>0 && newPoint.position.Y - initialSwipeY > yOutOfBoundsLength)))		// swipe up
		//{
		//	if (!swipeOutOfBoundDetected.empty()) {
		//		// ...
		//	}
		//}
		return false;
	}

}
