#include "stdafx.h"

#include "TemplateWindow.h"
#include "NativeScene.h"

#include <QtOpenGL/QGLWidget>

TemplateWindow::TemplateWindow(QWidget *parent) :
	QMainWindow(parent),
	m_pScene(0)
{
	ui.setupUi(this);

	// Init the main view for the scene using OpenGL. Always use double buffering and VSync.
	// Changing the format later is really problematic with Qt.
	QGLFormat qglFormat(QGL::SampleBuffers);
	qglFormat.setDoubleBuffer(true);
	qglFormat.setSwapInterval(1);

	QGLWidget* glWidget = new QGLWidget(qglFormat, ui.graphicsView);
	ui.graphicsView->setViewport(glWidget);
	ui.graphicsView->setFrameShape(QFrame::NoFrame);

	m_pScene = new NativeScene(ui.graphicsView);
	ui.graphicsView->setScene(m_pScene);
}


TemplateWindow::~TemplateWindow()
{
	// Do not delete m_pScene, its parent is ui.graphicsView, so it will be destroyed automatically
}


void TemplateWindow::resizeEvent(QResizeEvent* event)
{
	super::resizeEvent(event);

	m_pScene->setSceneRect(QRectF(QPointF(), ui.graphicsView->viewport()->size()));
	m_pScene->onAfterSetSceneRect(m_pScene->sceneRect());
}

void TemplateWindow::forceRecognize()
{
	if (m_pScene)
		m_pScene->recognizeManually();
}

UEditorMsgEater::UEditorMsgEater(QObject *parent /*= 0*/) : QObject(parent)
{
	uMsg = RegisterWindowMessage(L"Metaio_Kinect_RecognizeEngineMessage");
}

bool UEditorMsgEater::nativeEventFilter(const QByteArray &eventType, void *message, long *result)
{
	MSG* msg = reinterpret_cast<MSG*>(message);
	if (msg->message == uMsg) {
		QCoreApplication::instance()->quit();
		return true;
	}
	return false;
}
