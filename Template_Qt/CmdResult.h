#pragma once
#include "insNetwork\iInsTcpClient.h"
class CmdResult :
	public IInsTcpClientEvent
{
public:
	CmdResult();
	virtual ~CmdResult();

public:
	// 收到数据
	virtual void OnRecvData(char * apPackData, int aiPackLen);

	// 断开连接
	virtual void OnDisconnect(void);

	// 解析数据
	//bool retrieveJson(QJsonValue* json);

	// 设置同步条件变量
	//void setWaitCondition(QSharedPointer<QWaitCondition>& condition);

private:
	//bool convertResultToJson();
	//bool testIfTrackStopPlayingMsg();

private:
	//QByteArray  result;
	//QSharedPointer<QWaitCondition> condition;

	bool isJsonResult;
	//QJsonValue	jsonResult;
};

