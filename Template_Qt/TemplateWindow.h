#ifndef TEMPLATEWINDOW_H
#define TEMPLATEWINDOW_H

#include "ui_TemplateWindow.h"
#include "SceneBase.h"

#include <QtWidgets/QMainWindow>
#include <QtCore/QAbstractNativeEventFilter>

class UEditorMsgEater : public QObject, public QAbstractNativeEventFilter
{
	Q_OBJECT
public:
	UEditorMsgEater(QObject *parent = 0);
protected:
	virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *result) override;

	UINT uMsg;
};

class TemplateWindow : public QMainWindow
{
	typedef QMainWindow super;

	Q_OBJECT

public:
	TemplateWindow(QWidget *parent = 0);
	~TemplateWindow();

	// 当引擎程序隐藏界面在后台运行时，需要调用这个函数强制引擎工作
	void forceRecognize();

private:
	// From QMainWindow
	virtual void resizeEvent(QResizeEvent* event) override;

	Ui::TemplateWindowClass		ui;

	SceneBase*					m_pScene;
};

#endif // TEMPLATEWINDOW_H
