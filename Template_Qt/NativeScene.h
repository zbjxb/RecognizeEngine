#ifndef NATIVESCENE_H
#define NATIVESCENE_H

#include "SceneBase.h"

/// Implementation of non-AREL scene
class NativeScene : public SceneBase
{
	typedef SceneBase super;

	Q_OBJECT

public:
	NativeScene(QObject *parent = 0);
	virtual ~NativeScene();

private:
	/*metaio::IGeometry* m_pFishImagePlane;
	metaio::IGeometry* m_pCarotaImagePlane;*/
	/*metaio::IGeometry* m_earth;
	metaio::IGeometry* m_earthOcclusion;
	metaio::IGeometry* m_earthIndicators;
	bool m_earthOpened;*/

	virtual void loadContent() override;

	virtual void performRendering() override;

	//virtual void onGeometryTouched(metaio::IGeometry* geometry);

	virtual void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* mouseEvent) override;

	//virtual void onAnimationEnd(metaio::IGeometry* geometry, const metaio::stlcompat::String& animationName) override;

	virtual void onTrackingEvent(const metaio::stlcompat::Vector< metaio::TrackingValues > &trackingValues) override;
};

#endif