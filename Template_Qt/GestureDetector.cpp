#include "StdAfx.h"
#include "GestureDetector.h"
#include <exception>
#include <functional>
#include <QtCore/qlogging.h>
#include "insNetwork/iInsTcpClient.h"
#include "insApi.h"

namespace RecognizeEngine
{
	GestureDetector::GestureDetector(IKinectSensor* sensor)
	{
		if (sensor == nullptr)
			throw std::invalid_argument("null sensor");

		HRESULT hr;

		gestureDatabase          = L"jump.gbd";
		jumpGestureName          = L"Fly";

		// create the vbg source. The associated body tracking ID will be set when a valid body frame arrives from the sensor.
		hr = CreateVisualGestureBuilderFrameSource(sensor, 0, &vgbFrameSource);
		if (FAILED(hr))
			throw std::runtime_error("cannot create VisualGestureBuilder frame source");
		vgbFrameSource->SubscribeTrackingIdLost(&m_hTrackingIdLostEvent);

		// open the reader for the vgb frames
		hr = vgbFrameSource->OpenReader(&vgbFrameReader);
		if (FAILED(hr))
			throw std::runtime_error("cannot open VisualGestureBuilder frame reader");
		vgbFrameReader->put_IsPaused(true);
		//vgbFrameReader->SubscribeFrameArrived(&m_hGestureFrameArrivedEvent);	// 禁用机器学习模块。使用update内部状态变化来识别jump

		// load the gestures from the gesture database
		{
			CComPtr<IVisualGestureBuilderDatabase> database;

			TCHAR path[MAX_PATH] = { 0 };
			_tcscat_s(path, MAX_PATH, AssetPath());
			_tcscat_s(path, MAX_PATH - _tcslen(AssetPath()), gestureDatabase.c_str());
			hr = CreateVisualGestureBuilderDatabaseInstanceFromFile(path, &database);
			if (FAILED(hr))
				throw std::runtime_error("cannot open VisualGestureBuilder database");
			UINT gesturesCount;
			hr = database->get_AvailableGesturesCount(&gesturesCount);
			if (FAILED(hr)) {
				throw std::runtime_error("cannot get gestures count from database");
			}
			m_nGesturesCount = gesturesCount;

			IGesture** gestures = new IGesture*[gesturesCount];
			for (UINT i = 0; i != gesturesCount; i++) {
				gestures[i] = nullptr;
			}
			m_arrayGestures = gestures;

			hr = database->get_AvailableGestures(gesturesCount, gestures);
			if (FAILED(hr))
				throw std::runtime_error("cannot get available gestures from database");
			
			/*TCHAR gestureName[50];
			for (UINT i = 0; i != gesturesCount; i++)
			{
				memset(gestureName, 0, 50*sizeof(TCHAR));
				hr = gestures[i]->get_Name(50U, gestureName);
				if (FAILED(hr))
					throw std::runtime_error("cannot get gesture name from database");
				if (gestureName == jumpGestureName || gestureName == halfCrouchGestureName)
				{
					hr = vgbFrameSource->AddGesture(gestures[i]);
					if (FAILED(hr))
						throw std::runtime_error("cannot add gesture to frame source");
				}
			}*/

			hr = vgbFrameSource->AddGestures(gesturesCount, gestures);
			if (FAILED(hr))
				throw std::runtime_error("cannot add gestures to frame source");
		}

		GestureParameter leftFoot{ JointType_FootLeft, GestureDirection_Y, 0.3, 2, 1000 };
		GestureParameter rightFoot{ JointType_FootRight, GestureDirection_Y, 0.3, 2, 1000 };
		gestureMgr.RegisterGestureParamter(leftFoot);
		gestureMgr.RegisterGestureParamter(rightFoot);
		gestureMgr.RegisterSwipeDetected([this](){
			_tprintf(_T("id %I64u  %s found\n"), GetTrackingId(), _T("jump"));

			char szBuffer[1024];
			sprintf_s(szBuffer, 1024, \
				"{\"Operation\":\"EventTrigger\", \"Name\":\"%s\"}", "jump");

			std::wstring lwstrData;
			ins::AnsiToUnicode16(szBuffer, lwstrData);
			int iDataLen = ins::UnicodeToUTF8(lwstrData.c_str(), szBuffer, 1024);
			if (!TcpClient->SendData(szBuffer, iDataLen))
			{
				qCritical("SendData Error!");
				return ;
			}
		});

		m_bKeepCheckingEvent = true;
		m_hCheckEventThreadHandle = CreateThread(nullptr, 0, GestureDetector::CheckEventThreadProc, this, 0, nullptr);
	}

	GestureDetector::~GestureDetector()
	{
		StopCheckEvent();
		WaitForSingleObject(m_hCheckEventThreadHandle, 2000);
		CloseHandle(m_hCheckEventThreadHandle);

		gestureMgr.StopGesturePointTracking();

		if (m_arrayGestures)
		{
			for (UINT i = 0; i != m_nGesturesCount; i++) {
				SafeRelease(m_arrayGestures[i]);
			}
			delete[] m_arrayGestures;
		}

		//vgbFrameReader->UnsubscribeFrameArrived(m_hGestureFrameArrivedEvent);
		vgbFrameSource->UnsubscribeTrackingIdLost(m_hTrackingIdLostEvent);
	}

	void GestureDetector::Source_TrackingIdLost(ITrackingIdLostEventArgs* e)
	{
		// to do something ...
		gestureMgr.ResetGesturePoint();
	}

	void GestureDetector::Reader_GestureFrameArrived(IVisualGestureBuilderFrameArrivedEventArgs* e)
	{
		CComPtr<IVisualGestureBuilderFrameArrivedEventArgs> pe(e);

		HRESULT hr;
		//TCHAR gestureName[50];

		CComPtr<IVisualGestureBuilderFrameReference> frameRef;
		hr = pe->get_FrameReference(&frameRef);
		if (FAILED(hr))
			return;

		CComPtr<IVisualGestureBuilderFrame> frame;
		hr = frameRef->AcquireFrame(&frame);
		if (SUCCEEDED(hr) && frame)
		{
			// get the discrete gesture results which arrived with the latest frame
			for (UINT i = 0; i != m_nGesturesCount; i++)
			{
				/*memset(gestureName, 0, 50*sizeof(TCHAR));
				hr = m_arrayGestures[i]->get_Name(50, gestureName);*/

				GestureType gestureType;
				m_arrayGestures[i]->get_GestureType(&gestureType);

				if (/*gestureName == jumpGestureName &&*/ gestureType == GestureType_Discrete)
				{
					CComPtr<IDiscreteGestureResult> discreteGestureResult;
					hr = frame->get_DiscreteGestureResult(m_arrayGestures[i], &discreteGestureResult);
					if (SUCCEEDED(hr) && discreteGestureResult)
					{
						BOOLEAN isDetected;
						hr = discreteGestureResult->get_Detected(&isDetected);
						if (SUCCEEDED(hr) && isDetected) {
							float confidence;
							hr = discreteGestureResult->get_Confidence(&confidence);
							if (SUCCEEDED(hr) && confidence > 0.2f)
							{
								auto timenow = system_clock::now();
								static time_point<system_clock> lasttimeDetectJumpGesture = timenow;
								auto timespan = duration_cast<milliseconds>(timenow - lasttimeDetectJumpGesture).count();
								if (timespan < 1000 && timespan > 0)
									continue;

								_tprintf(_T("%s found\n"), /*gestureName*/jumpGestureName.c_str());

								std::string lstrName;
								ins::Unicode16ToAnsi(/*gestureName*/jumpGestureName.c_str(), lstrName);

								char szBuffer[1024];
								sprintf_s(szBuffer, 1024, \
									"{\"Operation\":\"EventTrigger\", \"Name\":\"%s\"}", /*lstrName.c_str()*/"jump");

								/*sprintf_s(szBuffer, 1024, \
								"{\"Operation\":\"SelectActor\", \"ObjectType\":\"%s\", \"Name\":\"%s\"}", \
								"CAMERA", "CameraActor");*/

								std::wstring lwstrData;
								ins::AnsiToUnicode16(szBuffer, lwstrData);
								int iDataLen = ins::UnicodeToUTF8(lwstrData.c_str(), szBuffer, 1024);
								if (!TcpClient->SendData(szBuffer, iDataLen))
								{
									qCritical("SendData Error!");
									break;
								}
							}
						}
					}

					continue;
				}
			}
		}
	}

	UINT64 GestureDetector::GetTrackingId() const
	{
		UINT64 trackingId;
		vgbFrameSource->get_TrackingId(&trackingId);
		return trackingId;
	}

	void GestureDetector::SetTrackingId(UINT64 id)
	{
		if (id != GetTrackingId())
			gestureMgr.StartGesturePointTracking();
		vgbFrameSource->put_TrackingId(id);
	}

	bool GestureDetector::IsPaused() const
	{
		BOOLEAN isPaused;
		vgbFrameReader->get_IsPaused(&isPaused);
		return isPaused;
	}

	void GestureDetector::SetPaused(bool val)
	{
		vgbFrameReader->put_IsPaused(val);
	}

	HRESULT GestureDetector::Update(IBody* body)
	{
		CComPtr<IBody> bodyPtr(body);
		if (!bodyPtr)
			return E_POINTER;

		HRESULT hr;

		hr = bodyPtr->GetJoints(JointType_Count, m_joints);
		if (FAILED(hr))
			return hr;

		gestureMgr.HandleGestureTracking(m_joints);

		return S_OK;
	}

	DWORD WINAPI GestureDetector::CheckEventThreadProc(LPVOID lpParam)
	{
		GestureDetector* detector = reinterpret_cast<GestureDetector*>(lpParam);
		if (!detector)
			return 1;

		HANDLE handles[] = {
			reinterpret_cast<HANDLE>(detector->m_hGestureFrameArrivedEvent),
			reinterpret_cast<HANDLE>(detector->m_hTrackingIdLostEvent)
		};

		CComPtr<IVisualGestureBuilderFrameReader> vgbFrameReader ( detector->VGBFrameReader() );
		CComPtr<IVisualGestureBuilderFrameSource> vgbFrameSource ( detector->VGBFrameSource() );

		while (detector->IsCheckingEvent())
		{
			switch (MsgWaitForMultipleObjects(_countof(handles), handles, false, 1000, QS_ALLINPUT))
			{
			case WAIT_OBJECT_0:	// gesture frame arrived event
			{
				CComPtr<IVisualGestureBuilderFrameArrivedEventArgs> pArgs;

				if (vgbFrameReader)
				{
					HRESULT hr = vgbFrameReader->GetFrameArrivedEventData(detector->m_hGestureFrameArrivedEvent, &pArgs);
					if (SUCCEEDED(hr))
					{
						detector->Reader_GestureFrameArrived(pArgs);
					}
				}
			}
			break;

			case WAIT_OBJECT_0 + 1:	// tracking id lost event
			{
				CComPtr<ITrackingIdLostEventArgs> pArgs;

				if (vgbFrameSource)
				{
					HRESULT hr = vgbFrameSource->GetTrackingIdLostEventData(detector->m_hTrackingIdLostEvent, &pArgs);
					if (SUCCEEDED(hr))
					{
						detector->Source_TrackingIdLost(pArgs);
					}
				}
			}
			break;

			default:
				break;
			}
		}

		return 0;
	}

	IVisualGestureBuilderFrameReader* GestureDetector::VGBFrameReader()
	{
		return vgbFrameReader;
	}

	IVisualGestureBuilderFrameSource* GestureDetector::VGBFrameSource()
	{
		return vgbFrameSource;
	}

}
